class BaseAdvertising:
    def __init__(self , clicks = 0 , views = 0 , id = 0):
        self.clicks = clicks
        self.views = views
        self.id = id
    def describeMe(self):
        return "I do basic advertising, sure."
    def getClicks(self):
        return self.clicks
    def getViews(self):
        return self.views