from base_model import *
class Advertiser(BaseAdvertising):
	totalClicks = 0
	def __init__(self, id, name):
		BaseAdvertising.__init__(self)
		self.id = id
		self.name = name

	def setName(self , name):
		self.name = name

	def getName(self):
		return self.name

	def describeMe(self):
		return "I'm an advertiser. My name is " + self.name + "."

	def incClicks(self):
		self.clicks += 1
		Advertiser.totalClicks += 1
	def incViews(self):
		self.views += 1
	def help():
		s = "setName(name) to set the name.\n"
		s += "getName() to get the name.\n"
		s += "getClicks() and getViews() to get the clicks or views.\n"
		s += "incClicks() and incViews() to increase clicks or views by 1.\n"
		s += "describeMe() for description.\n"
		s += "getTotalClicks() for getting the total number of clicks.\n"
		s += "and of course, help() to get help."
		return s
	def getTotalClicks():
		return Advertiser.totalClicks