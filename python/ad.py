from base_model import *
from advertiser import *
class Ad(BaseAdvertising):
	def __init__(self, id, title, imgUrl, link, advertiser):
		BaseAdvertising.__init__(self)
		self.id = id
		self.title = title
		self.imgUrl = imgUrl
		self.link = link
		self.advertiser = advertiser

	def getTitle(self):
		return self.title

	def setTitle(self , title):
		self.title = title

	def getImgUrl(self):
		return self.imgUrl

	def setImgUrl(self , imgUrl):
		self.imgUrl = imgUrl

	def getLink(self):
		return self.link

	def setLink(self , link):
		self.link = link

	def setAdvertiser(self , advertiser):
		self.advertiser = advertiser

	def describeMe(self):
		s = "I'm an ad and my title is " + self.title + ".\n"
		s += "My advertiser is " + self.advertiser.getName() + "."
		return s;

	def incClicks(self):
		self.clicks += 1
		self.advertiser.incClicks()

	def incViews(self):
		self.views += 1
		self.advertiser.incViews()


