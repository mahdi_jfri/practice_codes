#include<bits/stdc++.h>

using namespace std;

#include "base_model.cpp"
#include "advertiser.cpp"
#include "ad.cpp"

int Advertiser::totalClicks = 0;

int main()
{
	BaseAdvertising baseAdvertising;
	Advertiser advertiser1(1 , "name1");
	Advertiser advertiser2(2 , "name2");

	Ad ad1(1 , "title1" , "img-url1" , "link1" , advertiser1);
	Ad ad2(2 , "title2" , "img-url2" , "link2" , advertiser2);

	cout << baseAdvertising.describeMe() << endl;
	cout << ad2.describeMe() << endl;
	cout << advertiser1.describeMe() << endl;

	ad1.incViews();
	ad1.incViews();
	ad1.incViews();
	ad1.incViews();

	ad2.incViews();

	ad1.incClicks();
	ad1.incClicks();
	ad2.incClicks();

	cout << advertiser2.getName() << endl;
	advertiser2.setName("new name");
	cout << advertiser2.getName() << endl;

	cout << ad1.getClicks() << endl;
	cout << advertiser2.getClicks() << endl;

	cout << Advertiser::getTotalClicks() << endl;
	cout << Advertiser::help() << endl;
}



