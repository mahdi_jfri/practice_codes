class Ad: public BaseAdvertising
{
	private:
		string title , imgUrl , link;
		Advertiser* advertiser;
	public:
		Ad()
		{
		}
		Ad(int id_ , string title_ , string imgUrl_ , string link_ , Advertiser &advertiser_)
		{
			__setId(id_);
			title = title_;
			imgUrl = imgUrl_;
			link = link_;
			advertiser = &advertiser_;
		}
		string getTitle()
		{
			return title;
		}
		void setTitle(string title_)
		{
			title = title_;
		}
		string getImgUrl()
		{
			return imgUrl;
		}
		string getLink()
		{
			return link;
		}
		void setLink(string link_)
		{
			link = link_;
		}
		void setAdvertiser(Advertiser &advertiser_)
		{
			advertiser = &advertiser_;
		}
		string describeMe()
		{
			string s = "I'm an ad and my title is " + title + ".\n";
			s += "My advertiser is " + (*advertiser).getName() + ".";
			return s;
		}
		void incClicks()
		{
			__incClicks();
			(*advertiser).incClicks();
		}
		void incViews()
		{
			__incViews();
			(*advertiser).incViews();
		}
};
