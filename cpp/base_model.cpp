class BaseAdvertising
{
	int id , clicks , views;
	protected:
		void __setId(int id_)
		{
			id = id_;
		}
		void __incClicks()
		{
			clicks++;
		}
		void __incViews()
		{
			views++;
		}
	public:
		BaseAdvertising()
		{
			clicks = views = 0;
		}
		virtual string describeMe()
		{
			return "I do basic advertising, sure.";
		}
		int getClicks()
		{
			return clicks;
		}
		int getViews()
		{
			return views;
		}
};
