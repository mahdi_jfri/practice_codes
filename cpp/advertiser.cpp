class Advertiser: public BaseAdvertising
{
	string name;
	static int totalClicks;
	public:
		Advertiser()
		{
		}
		Advertiser(int id_ , string name_)
		{
			name = name_;
			__setId(id_);
		}
		void setName(string newname)
		{
			name = newname;
		}
		string getName()
		{
			return name;
		}
		void incClicks()
		{
			__incClicks();
			totalClicks++;
		}
		void incViews()
		{
			__incViews();
		}
		static string help()
		{
			string s;
			s += "setName(name) to set the name.\n";
			s += "getName() to get the name.\n";
			s += "getClicks() and getViews() to get the clicks or views.\n";
			s += "incClicks() and incViews() to increase clicks or views by 1.\n";
			s += "describeMe() for description.\n";
			s += "getTotalClicks() for getting the total number of clicks.\n";
			s += "and of course, help() to get help.";
			return s;
		}
		string describeMe()
		{
			string s = "I'm an advertiser. My name is " + name + ".";
			return s;
		}
		static int getTotalClicks()
		{
			return totalClicks;
		}
};
